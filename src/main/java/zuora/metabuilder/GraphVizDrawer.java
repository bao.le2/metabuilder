package zuora.metabuilder;

import static guru.nidi.graphviz.model.Factory.*;

import java.io.File;
import java.io.IOException;

import guru.nidi.graphviz.attribute.Attributes;
import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.attribute.Font;
import guru.nidi.graphviz.attribute.Rank;
import guru.nidi.graphviz.attribute.Rank.RankDir;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Graph;

import java.awt.*;
import javax.swing.*;

import com.github.jabbalaci.graphviz.GraphViz;

@SuppressWarnings("serial")
public class GraphVizDrawer extends JFrame {
	public GraphVizDrawer(String fileName) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 200);
		JPanel panel = new JPanel();
		
		// panel.setSize(500,640);
		panel.setBackground(java.awt.Color.WHITE);
		ImageIcon icon = new ImageIcon(fileName);
		JLabel label = new JLabel();
		label.setIcon(icon);
		panel.add(label);
		
		JScrollPane scrollPane=new JScrollPane(panel, 
				   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,  
				   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		this.getContentPane().add(scrollPane);

//		this.getContentPane().add(panel);
	}

	public static Graph getGraph1() {
		Graph g = graph("example1")
				.directed()
				.graphAttr()
				.with(Rank.dir(RankDir.LEFT_TO_RIGHT))
				.nodeAttr()
				.with(Font.name("arial"))
				.linkAttr().with("class", "link-class")
				.with(node("a").with(Color.RED).link(node("b")),
						node("b").link(to(node("c")).with(Attributes.attr("weight", 5), Style.DASHED)));
		return g;
	}

	public static void doTest1() throws IOException {
		Graphviz.fromGraph(getGraph1())
			.height(100)
			.render(Format.PNG)
			.toFile(new File("/Users/tanmoyd/work/gv/ex1.png"));
		new GraphVizDrawer("/Users/tanmoyd/work/gv/ex1.png").setVisible(true);
	}

	public static void doTest2() throws IOException {
		String input = "/private/var/folders/bh/m1vjl3h927ggdg_c3cm_982xf2h7f6/T/classMap6606702504643754135.gv";

		GraphViz gv = new GraphViz();
		gv.readSource(input);
		System.out.println(gv.getDotSource());

	    String type = "png";
		String repesentationType= "dot";
		
		File out = File.createTempFile("classMap", ".png");
		try {
			gv.writeGraphToFile( gv.getGraph(gv.getDotSource(), type, repesentationType), out );
			new GraphVizDrawer(out.getPath()).setVisible(true);
		} finally {
		}
	}
	
	public static void main(String[] args) throws IOException {
		doTest2();
	}
}
