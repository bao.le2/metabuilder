package zuora.metabuilder;

import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.*;

import com.github.jabbalaci.graphviz.GraphViz;


@SuppressWarnings("serial")
public class Driver extends JFrame {
	
	public Driver(String fileName) {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(3200, 2000);
		JPanel panel = new JPanel();
		
		//panel.setSize(2000, 1600);
		panel.setBackground(java.awt.Color.WHITE);
		ImageIcon icon = new ImageIcon(fileName);
		JLabel label = new JLabel();
		label.setIcon(icon);
		panel.add(label);
		
		JScrollPane scrollPane=new JScrollPane(panel, 
				   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,  
				   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		this.getContentPane().add(scrollPane);
	}

	public static void main(String[] args) {
		org.apache.log4j.Logger log = Logger.getRootLogger();
		log.setLevel(Level.FATAL);

		String schema  = "main";
		//root directory for billing project
		String rootDir = "/Users/ble/IntelliJ/billing";
		String sideCarDescriptorDir = "/Users/ble/IntelliJ/sidecar/sidecar-db/src/main/resources/descriptors";
		Meta meta   = new Meta();
		Meta metaDB = new Meta();
		
		ClassContinuum cc = new ClassContinuum();
		String regex = "ZB_SUBSCRIPTION[_a-zA-Z]*";
		
		try {
			long start, end;
			
			// First load from database
			MetaDBLoader.LoadFromDatabase(metaDB, schema);
			
			// Load definitions from the Hibernate files
			MetaHibernateLoader.LoadFromHibernateFiles(meta, rootDir, null);

			// Then Load & Resolve Annotations in the jars
			MetaAnnotationLoader.LoadFromAnnotations(meta, cc, rootDir, null);

			// Merge the Metas
			SecondPassCompile.mergeMetas(meta, metaDB);
			
			// Then Load the details of the Business Objects
			MetaViewLoader.LoadFromViewDefinitions(meta, sideCarDescriptorDir);
			
			// Now resolve all the details and generate the reference map
			File  gvFile = 	File.createTempFile("classMap", ".gv");

			// Uncomment on Check-in
//			gvFile.deleteOnExit();
			System.out.printf("Writing Reference Map to [%s]\n", gvFile.getPath());
			SecondPassCompile.compile(meta, metaDB, gvFile, regex);
			
			// Generate graph for viewing
			try {
				GraphViz gv = new GraphViz();
				
				String gvFileName = gvFile.getPath().toString();
				gv.readSource(gvFileName);

				File out = File.createTempFile("classMap", ".png");
				
				// Uncomment on check-in
//				out.deleteOnExit();
				System.out.printf("Reading graph from: [%s], writing to: [%s]\n", gvFile.getPath(), out.getPath());
				
			    String type = "png";
				String repesentationType= "dot";

				gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, repesentationType), out);
				new Driver(out.getPath()).setVisible(true);
			} finally {
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
