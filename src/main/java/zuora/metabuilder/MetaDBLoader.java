package zuora.metabuilder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import zuora.metabuilder.Meta.ColumnInfo;
import zuora.metabuilder.Meta.TableInfo;


public class MetaDBLoader {
	private static String sqlForExtractingColumns =   
			"select \n" + 
			"    c.TABLE_SCHEMA,  \n" + 
			"    c.TABLE_NAME,  \n" + 
			"    c.COLUMN_NAME,  \n" + 
			"    c.ORDINAL_POSITION,  \n" + 
			"    c.COLUMN_TYPE,\n" + 
			"    case when k.CONSTRAINT_NAME like 'PRIMARY%%' then 1 else 0 end as PRIMARY_KEY, \n" + 
			"    1\n " +  
			"from      \n" + 
			"    information_schema.columns c \n" + 
			"    inner join INFORMATION_SCHEMA.TABLES t on c.TABLE_NAME = t.TABLE_NAME and c.TABLE_SCHEMA = t.TABLE_SCHEMA \n" + 
			"    left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE k  on k.COLUMN_NAME = c.COLUMN_NAME and k.TABLE_NAME = c.TABLE_NAME\n" + 
			"where c.TABLE_SCHEMA = \'%s\'" + 
			"order by c.TABLE_SCHEMA, c.TABLE_NAME;";

	public static Connection connect() {
		try {
			com.mysql.cj.jdbc.Driver driver = new com.mysql.cj.jdbc.Driver();

			Properties props = new Properties();
			Connection conn = driver.connect("jdbc:mysql://127.0.0.1:3306?user=root&password=", props);
			return conn;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	static void LoadFromDatabase(Meta meta, String schema) throws SQLException {
		long start = System.nanoTime();
		long end = System.nanoTime();
		
		int tableCount = 0;
		try (Connection conn = connect()) {
			String formattedSQL = String.format(sqlForExtractingColumns, schema);
			ResultSet rs = conn.createStatement().executeQuery(formattedSQL);
			while (rs.next()) {
				String tabName = rs.getString(2);
				String colName = rs.getString(3);
				Integer colPos = rs.getInt(4);
				String colType = rs.getString(5);
				Integer isPK   = rs.getInt(6);
								
				ColumnInfo colInfo = new ColumnInfo(tabName, colName, colType, colPos, isPK == 1);
				
				// Build table to column mapping
				TableInfo tabInfo = meta.tableMap.get(tabName);
				if (tabInfo == null) {
					tabInfo = new TableInfo(tabName, schema);
					meta.tableMap.put(tabName, tabInfo);
					tableCount++;
				}
				tabInfo.colMap.put(colName, colInfo);
				if (colInfo.isPrimaryKey)
					tabInfo.pkColumns.add(colName);
				
				// Build Reverse-index for column to table mapping
				Set<String> tsForThisColumn = meta.colTableSetMap.get(colName);
				if (tsForThisColumn == null) {
					tsForThisColumn = new HashSet<>();
					meta.colTableSetMap.put(colName, tsForThisColumn);
				}
				tsForThisColumn.add(tabName);
			}
			end = System.nanoTime();
			System.out.printf("Loaded %d tables from schema: %s in %8.3f ms\n", tableCount, schema, ((double)end - (double)start)/1e6);
		}
	}
}
