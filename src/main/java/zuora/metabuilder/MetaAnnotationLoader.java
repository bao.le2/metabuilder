package zuora.metabuilder;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Inherited;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Entity;

import javassist.bytecode.*;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.TypeDef;

import org.reflections.Reflections;
import org.reflections.Store;
import org.reflections.scanners.AbstractScanner;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.google.common.base.Strings;

import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.AnnotationMemberValue;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import zuora.metabuilder.Meta.ColumnInfo;
import zuora.metabuilder.Meta.State;
import zuora.metabuilder.Meta.TableInfo;
import zuora.metabuilder.Meta.Typedef;
import zuora.metabuilder.Utilities.Tuple;
import javassist.bytecode.annotation.ClassMemberValue;

public class MetaAnnotationLoader {

	public static class GenericScanner extends AbstractScanner {
		final ClassContinuum cc;
		public GenericScanner(ClassContinuum cc) {
			this.cc = cc;
		}

		public void scan(Object cls, Store store) {
			ClassFile clsFile = (ClassFile)cls;
			String className = clsFile.getName();
			if (className.endsWith("NegativeInvoiceMigrationMonitor"))
				Utilities.Debug();
			if (!cc.isMapped(clsFile.getName()))
				cc.putAugmentedClass(className, clsFile);
		}

		public void scan(Object o) {

		}
	}

	// Scanner
	public static class SideEffectingTypeAnnotationsScanner extends TypeAnnotationsScanner {
		static String[] _interestingTableAnnotations = { 
				Entity.class.getName(), 
				Table.class.getName(),
				MappedSuperclass.class.getName(),
				TypeDefs.class.getName(),
				TypeDef.class.getName()
		};
		static String[] _interestingColumnAnnotations = { 
				Basic.class.getName(), 
				Column.class.getName(), 
		};

		Set<String> interestingTableAnnotations = new HashSet<>();
		Set<String> interestingColumnAnnotations = new HashSet<>();

		final ClassContinuum cc;
		final Meta meta;

		public SideEffectingTypeAnnotationsScanner(Meta meta, ClassContinuum cc) {
			for (String ia : _interestingTableAnnotations)
				interestingTableAnnotations.add(ia);
			for (String ia : _interestingColumnAnnotations)
				interestingColumnAnnotations.add(ia);
			this.meta = meta;
			this.cc = cc;
		}

		private void extractManyOneAnnotationProps(TableInfo ti, FieldInfo field, Annotation annotation,
				Map<String, Map<String, Object>> annoProps, Map<String,String> overrideMap) {
			// Just capture the fact that there is a many-to-one annotation
			HashMap<String, Object> annoProp = new HashMap<>();
			annoProp.put(Meta.c_ManyOneClassProperty, null);
			annoProps.put(Meta.c_ManyOneAnnotationProperty, annoProp);
		}

		private void extractColumnAnnotationProps(TableInfo ti, FieldInfo field, Annotation annotation,
				Map<String, Map<String, Object>> annoProps, Map<String,String> overrideMap) {
			
			// Pick up the column name.
			// Try annotation first. If it does not exist, then try member name
			Map<String, Object> colProps = new HashMap<>();
			
			MemberValue mv = annotation.getMemberValue("name");
			String colName = mv != null ? 
									((StringMemberValue) (mv)).getValue(): 
									overrideMap != null && overrideMap.containsKey(field.getName()) ?
											overrideMap.get(field.getName()) :
											Utilities.propertyToColumnName(field.getName());
			colProps.put(Meta.c_NameProperty, colName);
			annoProps.put(Meta.c_ColumnNameAnnotationProperty, colProps);
		}

		private void extractIdAnnotationProps(TableInfo ti, FieldInfo field, Annotation annotation,
				Map<String, Map<String, Object>> annoProps, Map<String,String> overrideMap) {
			// Pick up the column name
			Map<String, Object> colProps = new HashMap<>();
			MemberValue mv = annotation.getMemberValue("name");
			String colName = mv != null ? 
									((StringMemberValue) (mv)).getValue() : 
									overrideMap != null && overrideMap.containsKey(field.getName()) ?
											overrideMap.get(field.getName()) :
											Utilities.propertyToColumnName(field.getName());
			
			colProps.put(Meta.c_NameProperty, colName);
			ti.pkColumns.add(colName);
			annoProps.put(Meta.c_ColumnNameAnnotationProperty, colProps);
		}

		private void extractTypeAnnotationProps(TableInfo ti, FieldInfo field, Annotation annotation,
				Map<String, Map<String, Object>> annoProps, Map<String,String> overrideMap) {
			// Pick up the column type
			Map<String, Object> colProps = new HashMap<>();
			MemberValue mv = annotation.getMemberValue("type");
			String colType = mv != null ? 
									((StringMemberValue) (mv)).getValue() : 
									null;
			
			if (!Strings.isNullOrEmpty(colType))
				colProps.put(Meta.c_TypeProperty, colType);
			annoProps.put(Meta.c_TypeAnnotationProperty, colProps);
		}

		
		private String getFieldTypeNameFromFieldInfoOld(FieldInfo fi) {
			String _type = fi.getDescriptor();
			String type = "";
			switch (_type.charAt(0)) {
			case 'L':
				type = _type.substring(1);
				type = type.replace('/', '.');
				if (type.endsWith(";"))
					type = type.substring(0, type.length() - 1);
				return type;
			case 'Z':
				return Boolean.class.getName();
			case 'I':
				return Integer.class.getName();
			case 'J':
				return Long.class.getName();
			case 'S':
				return Short.class.getName();
			default:
				Utilities.Debug();
			}
			return type;
		}

		private String getFieldTypeNameFromFieldInfo(ClassFile clsFile, FieldInfo fi) {
			try {
				String className = clsFile.getName();
				Class<?> clazz = Class.forName(className);
				Class<?> fieldClass = Utilities.getClassFromField(clazz, fi.getName());
				return fieldClass.getName();
			} catch (ClassNotFoundException | NoClassDefFoundError ex) {
//				if (!clsFile.getName().equals("com.zuora.paymentgateway.model.GatewayTransaction"))
//					System.out.println(clsFile.getName());
				return getFieldTypeNameFromFieldInfoOld(fi);
			}
		}

		private String translateTypeFromTypedefs(String annoType, List<Typedef> typedefs) {
			if (Strings.isNullOrEmpty(annoType))
				return null;
			
			String ret = null;
			if (typedefs != null) {
				for (Typedef td : typedefs) {
					if (td.name.equals(annoType) && !Strings.isNullOrEmpty(td.clazz)) {
						ret = td.clazz;
						if (td.params != null && td.params.get("name") != null) {
							ret = td.params.get("name");
							break;
						}
					}
				}
			}
			return ret;
		}
		
		private void processField(ClassFile clsFile, TableInfo ti, FieldInfo field,
				Map<String, Map<String, Object>> annoProps, Map<String,String> override,
				List<Typedef> typedefs) {

			// Resolve Column Name
			Map<String, Object> colInfoAnnotation = annoProps.get(Meta.c_ColumnNameAnnotationProperty);
			String colName;
			if (colInfoAnnotation == null || colInfoAnnotation.size() == 0) {
				colName = override != null && override.containsKey(field.getName()) ?
								override.get(field.getName()) :
								Utilities.propertyToColumnName(field.getName());
			} else 
				colName = (String) colInfoAnnotation.get(Meta.c_NameProperty);
			
			if (Diagnostics.trackClass(clsFile.getName(), null))
				Utilities.Debug();
			
			// Resolve the type
			// 1. Is there a type qualifier for the field?
			String javaType = null;
			String annoType = null;
			Map<String, Object> typeAnnotation = annoProps.get(Meta.c_TypeAnnotationProperty);
			if (typeAnnotation != null && typeAnnotation.containsKey(Meta.c_TypeProperty))
				annoType = (String)typeAnnotation.get(Meta.c_TypeProperty);
			
			// 2. The type defined for the field
			javaType = getFieldTypeNameFromFieldInfo(clsFile, field);
			Map<String, Object> manyOneAnnotation = annoProps.get(Meta.c_ManyOneAnnotationProperty);
			if (manyOneAnnotation != null) {
				Object manyOneAnnoProp = manyOneAnnotation.get(Meta.c_ManyOneClassProperty);
				if (manyOneAnnoProp == null)
					manyOneAnnotation.put(Meta.c_ManyOneClassProperty, javaType);
			}
			
			// 3. Is there a typedef mapping for the class. See if we need to translate
			String resolved = translateTypeFromTypedefs(annoType, typedefs);
			if (resolved != null)
				javaType = resolved;
			
			Tuple<Boolean, ColumnInfo> r = Utilities.getOrCreateColumnInfo(ti, colName, javaType, annoProps, State.ResolvedByAnnotation, clsFile.getName());
			ColumnInfo ci = r.b;
			ci.colType = annoType;
			ci.rawJavaClass = javaType;
			
			if (ti.pkColumns.contains(colName))
				ci.isPrimaryKey = true;

			// Print
			if (_debug) {
				System.out.printf("\t\t(Name: %s), (JavaClass: %s), (SqlType: %s), (IsPrimaryKey): %s\n", 
						ci.colName,
						ci.javaClass,
						ci.colType,
						ci.isPrimaryKey ? "true" : "false");
			}			
		}

		// Skip some default fields
		static String[] _fieldsToSkip = new String[] { "serialVersionUID", };
		static Set<String> fieldsToSkip = new HashSet<>();
		static {
			for (String f : _fieldsToSkip)
				fieldsToSkip.add(f);
		}

		// Used for diagnostic debugging
		boolean _debug = false;
		public void DebugOn (String className) { _debug = true; }
		public void DebugOff(String className) { _debug = false; }
		
		// Pick up typedef annotations for the class
		public List<Typedef> getTypedefAnnotations(ClassFile cls, Annotation annotation) {
			List<Typedef> typedefList = new ArrayList<>();
			ArrayMemberValue _mv = (ArrayMemberValue)annotation.getMemberValue("value");
			if (_mv != null) {
				MemberValue[] mvA = _mv.getValue();
				for (MemberValue _mva : mvA) {
					AnnotationMemberValue mv = (AnnotationMemberValue)_mva;
					Annotation a = mv.getValue();
					String name = ((StringMemberValue)a.getMemberValue("name")).getValue();
					
					MemberValue typeMV = a.getMemberValue("typeClass");
					if (typeMV != null) {
						String className = ((ClassMemberValue)typeMV).getValue();
						Typedef td = new Typedef(name, className);
						typedefList.add(td);
						
						ArrayMemberValue _params = (ArrayMemberValue)a.getMemberValue("parameters");
						MemberValue[] params = _params != null ? _params.getValue() : null;
						if (params != null) {
							for (MemberValue _mvp : params) {
								AnnotationMemberValue mvp = (AnnotationMemberValue)_mvp;
								Annotation mvpa = mvp.getValue();
								String paramName = mvpa.getMemberValue("name") != null ?
										 		   ((StringMemberValue)mvpa.getMemberValue("name")).getValue():
										 			   null;
								if (paramName != null) {
									String paramValue = mvpa.getMemberValue("value") != null ?
									 		   ((StringMemberValue)mvpa.getMemberValue("value")).getValue():
									 			   null;
									if (paramValue != null) {
										td.params.put(paramName, paramValue);
									}
								}
							}
						}
					}
				}
			}
			return typedefList;
		}
		
		@SuppressWarnings({ "unchecked", "unused" })
		public Tuple<List<FieldInfo>, List<Typedef>> getFields(ClassFile cls) {
			List<FieldInfo> fields = new ArrayList<>();
			List<Typedef> typedefs = null;

			Boolean isMappedSuperClass = false, isEntity = false, isTable = false;
			
			// Pick up interesting annotations
			List<String> annotationsList = getMetadataAdapter().getClassAnnotationNames(cls);
			List<Annotation> annotations = new ArrayList<>();

			for (String annotationsString : annotationsList) {
				annotations.add(new Annotation(annotationsString, new ConstPool(annotationsString)));
			}

			for (Annotation annotation : annotations) {
				if (annotation.getTypeName().equals(Table.class.getName()))
					isTable = true;
				else if (annotation.getTypeName().equals(Entity.class.getName()))
					isEntity = true;
				else if (annotation.getTypeName().equals(MappedSuperclass.class.getName()))
					isMappedSuperClass = true;
				else if (annotation.getTypeName().equals(TypeDefs.class.getName()))
					typedefs = getTypedefAnnotations(cls, annotation);
			}

			// Now get the fields for this class. We will iterate over them later
			List<FieldInfo> classFields = getMetadataAdapter().getFields(cls);
			Set<String> fieldsToExclude = new HashSet<>();
						
			// For all class annotations, pick up fields that have the @Basic, @Version, @Id, @ManyToOne and @Column annotation
			Set<String> uniqueFieldNames = new HashSet<>();
			for (FieldInfo fi : classFields) {
				String fn = fi.getName();

				// Skip static fields
				if (fieldsToSkip.contains(fn) || 
					(fi.getAccessFlags() & AccessFlag.STATIC) != 0 ||
					(fi.getAccessFlags() & AccessFlag.TRANSIENT) != 0)
					continue;
				
				// if private validate if there is a get and set
				
				// pick up fields that have an annotation of interest
				List<String> fieldAnnotationsList = getMetadataAdapter().getFieldAnnotationNames(fi);
				List<Annotation> fieldAnnotations = new ArrayList<>();
				for (String fieldAnnotationString : fieldAnnotationsList) {
					fieldAnnotations.add(new Annotation(fieldAnnotationString, new ConstPool(fieldAnnotationString)));
				}
				boolean fPickUpField = true;
				for (Annotation fieldAnnotation : fieldAnnotations) {
					String annotName = fieldAnnotation.getTypeName();
					
					switch (annotName) {
					case "javax.persistence.OneToMany":
						fieldsToExclude.add(fi.getName());
						fPickUpField &= false;
						break;
					
					case "javax.persistence.Version":
					case "javax.persistence.ManyToOne":
					case "javax.persistence.Id":
					case "javax.persistence.JoinColumn":
					case "javax.persistence.Basic":
					case "javax.persistence.Column":
						fPickUpField &= true;
					}					
				}
				if (fPickUpField) {
					fields.add(fi);
					uniqueFieldNames.add(fi.getName());
				}
			}

			// Hack for now
			isMappedSuperClass = true;
			
			// Do some specific processing per the class level annotations
			if (isMappedSuperClass) {
				
				// For MappedSuperClasses, pick up all the methods that have a proper get-set
				Set<String> fieldsWithGetSet = new HashSet<>();
				List<MethodInfo> methods     = getMetadataAdapter().getMethods(cls);
				for (MethodInfo mi : methods) {
					String methodName = mi.getName();
					String field = "";
					if (methodName.startsWith("get") || methodName.startsWith("set"))
						field = methodName.substring(3);
					if (methodName.startsWith("is"))
						field = methodName.substring(2);
					if (!Strings.isNullOrEmpty(field)) {
						char[] _f = field.toCharArray();
						_f[0] = Character.toLowerCase(_f[0]);
						fieldsWithGetSet.add(new String(_f));
					}
				}
				
				for (FieldInfo fi : classFields) {
					String fn = fi.getName();
					
					// Skip static fields, or many-to-one fields which happen to be collections
					if (fieldsToSkip.contains(fn) || 
						(fi.getAccessFlags() & AccessFlag.STATIC) != 0 ||
						fieldsToExclude.contains(fn))
						continue;

					// Pick up fields that have a proper getter or setter 
					if (fieldsWithGetSet.contains(fn) && !uniqueFieldNames.contains(fn))
						fields.add(fi);
				}
			}
			
			// Diagnostics
			if (_debug) {
				System.out.printf("Fields picked up for class: [%s]: {%s}\n",
						cls.getName(),
						fields.stream().map(x->x.getName()).collect(Collectors.joining(", ")));
			}
			return new Tuple<>(fields, typedefs);
		}
		
		@SuppressWarnings("unchecked")
		public String getTableNameFromClass(ClassFile cls, Map<String, String> fieldOverrides) {
			List<String> annotationsList = getMetadataAdapter().getClassAnnotationNames(cls);
			List<Annotation> annotations = new ArrayList<>();

			for (String annotationsString : annotationsList) {
				annotations.add(new Annotation(annotationsString, new ConstPool(annotationsString)));
			}

			String tableName = "";

			for (Annotation annot : annotations) {

				// Get table name & if not specified resolve it using Zuora's naming conventions
				if (annot.getTypeName().equals(Table.class.getName())) {

					// Pick up the table name
					MemberValue _tableName = annot.getMemberValue("name");
					if (_tableName == null || (StringMemberValue) _tableName == null)
						continue;
					tableName = ((StringMemberValue) _tableName).getValue();
				}
				else if (annot.getTypeName().equals(AttributeOverrides.class.getName())) {
					ArrayMemberValue overRides = (ArrayMemberValue) annot.getMemberValue("value");
					if (overRides != null) {
						for (MemberValue mv : overRides.getValue()) {
							if (mv instanceof AnnotationMemberValue) {
								AnnotationMemberValue amv = (AnnotationMemberValue)mv;
								Annotation annotI = amv.getValue();
								if (annotI != null) {
									String sourceField = ((StringMemberValue)annotI.getMemberValue("name")).getValue();
									Annotation targetColAnnot = ((AnnotationMemberValue)annotI.getMemberValue("column")).getValue();
									if (targetColAnnot != null) {
										String targetCol = ((StringMemberValue)targetColAnnot.getMemberValue("name")).getValue();
										fieldOverrides.put(sourceField, targetCol);
									}
								}
							}
						}
					}
				}
			}
			return tableName;
		}
		
		@SuppressWarnings("unchecked")
		private void processFieldList(TableInfo ti, ClassFile thisCls, List<FieldInfo> fields, 
				Map<String,String> fieldToColOverrides, List<Typedef> typedefs) {
			
			// process the field
			if (Diagnostics.trackClass(thisCls.getName(), null))
				Utilities.Debug();
			
			for (FieldInfo field : fields) {
				boolean fSkip = false;
				
				// Iterate over annotations of the field, picking up interesting attributes
				Map<String, Map<String, Object>> annoProps = new HashMap<>();
				List<String> fieldAnnotationsList = getMetadataAdapter().getFieldAnnotationNames(field);
				List<Annotation> fieldAnnotations = new ArrayList<>();
				for (String fieldAnnotationString : fieldAnnotationsList) {
					fieldAnnotations.add(new Annotation(fieldAnnotationString, new ConstPool(fieldAnnotationString)));
				}
				
				if (fieldAnnotations == null || fieldAnnotations.size() == 0) {
					Map<String, Object> mP = new HashMap<>();
					String columnName = fieldToColOverrides != null && fieldToColOverrides.containsKey(field.getName()) ?
										fieldToColOverrides.get(field.getName()) :
										Utilities.propertyToColumnName(field.getName());
					mP.put(Meta.c_NameProperty, columnName);
					annoProps.put(Meta.c_ColumnNameAnnotationProperty, mP);
				} else {
					for (Annotation fieldAnnotation : fieldAnnotations) {
						String annotName = fieldAnnotation.getTypeName();

						switch (annotName) {
						case "javax.persistence.Transient":
						case "javax.persistence.OneToMany":
							fSkip = true;
							continue;
							
						case "javax.persistence.ManyToOne":
							extractManyOneAnnotationProps(ti, (FieldInfo) field, fieldAnnotation, annoProps, fieldToColOverrides);
							break;

						case "javax.persistence.Id":
							extractIdAnnotationProps(ti, (FieldInfo) field, fieldAnnotation, annoProps, fieldToColOverrides);
							break;

						case "javax.persistence.JoinColumn":
						case "javax.persistence.Basic":
						case "javax.persistence.Column":
							extractColumnAnnotationProps(ti, (FieldInfo) field, fieldAnnotation, annoProps, fieldToColOverrides);
							break;
							
						case "org.hibernate.annotations.Type":
							extractTypeAnnotationProps(ti, (FieldInfo) field, fieldAnnotation, annoProps, fieldToColOverrides);
							break;
						}
					}
				}
			
				// process the field
				if (Diagnostics.trackClass(thisCls.getName(), null))
					Utilities.Debug();
				if (!fSkip)
					processField((ClassFile) thisCls, ti, field, annoProps, fieldToColOverrides, typedefs);
			}
		}
	
		private void processFieldsFromHbm(TableInfo ti) {
			if (_debug) {
				System.out.printf("Fields picked up for class: [%s]: {%s}\n",
						ti.javaClass,
						ti.colMap.values().stream().map(x->x.colName).collect(Collectors.joining(", ")));
			}
			
			// Resolve column types to the base types of the target composite types
			for (ColumnInfo ci : ti.colMap.values()) {
				
				if (ci.colName.equals("operation"))
					Utilities.Debug();				
				
				
				if (Strings.isNullOrEmpty(ci.colType)) {
					String javaClass = ci.javaClass;
					if (meta.classMap.containsKey(javaClass)) {
						Tuple<String,String> baseType = Utilities.getBaseTypeForCompositeType(javaClass, meta, ci);
						if (baseType != null) {
							ci.javaClass = baseType.a;
							ci.colType = baseType.b;
						}
					}
				}
				if (_debug) {
					System.out.printf("\t\t(Name: %s), (JavaClass: %s), (SqlType: %s), (IsPrimaryKey): %s\n", 
							ci.colName,
							ci.javaClass,
							ci.colType,
							ci.isPrimaryKey ? "true" : "false");
				}
			}
		}
		
		@SuppressWarnings({ "unchecked" })
		public void processPendingClasses() {
			for (Map.Entry<String, TableInfo> me : meta.classMap.entrySet()) {
				TableInfo ti = me.getValue();
				String className = ti.javaClass;

				Boolean fIsTracking = Diagnostics.trackClass(className, (x)->{DebugOn(x);});
				if (fIsTracking)
					System.out.printf("--%s---------------------------------------\n", className);
				
				if (!Strings.isNullOrEmpty(className)) {
					processFieldsFromHbm(ti);
					ClassFile thisCls = cc.lookupClass(className);
					while (true) {
						
						String superClass = getMetadataAdapter().getSuperclassName(thisCls);
						if (!Strings.isNullOrEmpty(superClass) &&
							(thisCls = cc.lookupClass(superClass)) != null) {
							
							Tuple<List<FieldInfo>, List<Typedef>> fieldInfo = getFields(thisCls);
							List<FieldInfo> fields = fieldInfo.a;
							List<Typedef> typedefs = fieldInfo.b;

							processFieldList(ti, thisCls, fields, null, typedefs);
						} else
							break;
						
					}
				}
				if (fIsTracking)
					System.out.printf("--%s---------------------------------------\n", ti.source);
				DebugOff(null);
			}
		}
		
		@SuppressWarnings({ "unchecked", "unused" })
		public void postProcessClasses() throws ClassNotFoundException {
			int classCount = 0;
			for (Map.Entry<String, ClassFile> me : cc.getMappedClasses().entrySet()) {
				++classCount;
				ClassFile clsJava = (ClassFile) me.getValue();
				String className = clsJava.getName();
				Map<String, String> fieldToColOverrides = new HashMap<>();
 				
				Boolean fIsTracking = Diagnostics.trackClass(className, (x)->{DebugOn(x);});
				if (fIsTracking)
					System.out.printf("--%s---------------------------------------\n", className);

				// Resolve tableName and annotations of interest
				String tableName = getTableNameFromClass(clsJava, fieldToColOverrides);
								
				ClassFile thisCls = clsJava;
				TableInfo ti = Utilities.getOrCreateTableInfo(meta, tableName, className, className);
				ti.state = State.ResolvedByAnnotation;
				
				do {
					Tuple<List<FieldInfo>, List<Typedef>> fieldInfo = getFields(thisCls);
					List<FieldInfo> fields = fieldInfo.a;
					List<Typedef> typedefs = fieldInfo.b;
					processFieldList(ti, thisCls, fields, fieldToColOverrides, typedefs);
					
					// Navigate the super class hierarchy and retrieve the field names
					String superClass = getMetadataAdapter().getSuperclassName(thisCls);
					thisCls = !Strings.isNullOrEmpty(superClass) ? (ClassFile)this.cc.lookupClass(superClass) : null;
					if (!Strings.isNullOrEmpty(superClass) && thisCls == null)
						Utilities.Debug();
				} while (thisCls != null);
				
				//Diagnostics
				if (_debug) {
					if (Diagnostics.trackClass(className, null))
						System.out.printf("Table:[%s], Class:[%s], Columns:[(%d) %s]\n",
								ti.tableName, ti.javaClass, ti.colMap.size(),
								ti.colMap.values().stream().map(x->x.colName).sorted().collect(Collectors.joining(", ")));
				}							
//				System.out.printf("Annotation: [%s] [%s] Properties:%d\n", ti.tableName, ti.javaClass, ti.colMap.size());
				if (fIsTracking)
					System.out.printf("--%s---------------------------------------\n", ti.source);
				DebugOff(null);
			}
		}
		
		private boolean isNameExplicitlySpecified(ColumnInfo ci) {
			boolean ret = false;
			if (ci.annotations != null && ci.annotations.size()> 0) {
				Map<String, Map<String, Object>> annotations = ci.annotations;
				if (annotations.containsKey(Meta.c_ColumnNameAnnotationProperty) && 
					annotations.get(Meta.c_ColumnNameAnnotationProperty) != null && 
					annotations.get(Meta.c_ColumnNameAnnotationProperty).containsKey(Meta.c_NameProperty) &&
					annotations.get(Meta.c_ColumnNameAnnotationProperty) != null &&
					annotations.get(Meta.c_ColumnNameAnnotationProperty).get(Meta.c_NameProperty) != null && 
					annotations.get(Meta.c_ColumnNameAnnotationProperty).get(Meta.c_NameProperty).equals(ci.colName))
					return true;
				if (annotations.containsKey(Meta.c_ManyOneAnnotationProperty) && 
					annotations.get(Meta.c_ManyOneAnnotationProperty) != null && 
					annotations.get(Meta.c_ManyOneAnnotationProperty).containsKey(Meta.c_NameProperty) &&
					annotations.get(Meta.c_ManyOneAnnotationProperty) != null &&
					annotations.get(Meta.c_ManyOneAnnotationProperty).get(Meta.c_NameProperty) != null && 
					annotations.get(Meta.c_ManyOneAnnotationProperty).get(Meta.c_NameProperty).equals(ci.colName))
					return true;
			}
			return ret;
		}
		
		@SuppressWarnings("unused")
		public void resolveColumnNames() {
			int tableCount = 0;
			for (Map.Entry<String, TableInfo> meT : meta.tableMap.entrySet()) {
				TableInfo ti = meT.getValue();
				tableCount++;
				
				if (Diagnostics.trackClass(ti.javaClass, null))
					Utilities.Debug();
				
				Diagnostics.trackClass(ti.javaClass, (x)->{DebugOn(x);});
				
				// Collect the fields in the database
				Map<String, ColumnInfo> newColInfos = new HashMap<>();
				Set<String> colsToRemove = new HashSet<>();
				for (Map.Entry<String, ColumnInfo> meC : ti.colMap.entrySet()) {
					ColumnInfo ci = meC.getValue();
					
					String javaClass = ci.javaClass;
					if (Strings.isNullOrEmpty(javaClass))
						continue;
					
					// tiTarget will not be found for primitive types
					TableInfo tiTarget = meta.classMap.get(ci.javaClass);
					if (tiTarget != null) {
						
						// This means the column at hand has a Compound Type. We need to get to the primitive.
						// TODO: do this recursively
						if (tiTarget.pkColumns.size() == 1) {
							
							// Get the type of the primary key for the Compound Type
							String pkColumn = Utilities.getAnElementFromSet(tiTarget.pkColumns);
							ColumnInfo ciTarget = tiTarget.colMap.get(pkColumn);
							
							// if name was explicitly specified then we will not translate
							boolean fWasExplicitForCI = isNameExplicitlySpecified(ci);
							
							// Compute the new column name
							String newColName = fWasExplicitForCI ? ci.colName :
													ci.colName.endsWith("_id") ? ci.colName : 
														ci.colName + "_" + ciTarget.colName;
							
							// Compute the new Type
							String newJavaClass = ciTarget.javaClass;
							String newColType = ciTarget.colType;
							
							// Check if the column-name was changed
							if (!fWasExplicitForCI) {
								// Since we are iterating we cannot change the colMap in place.
								// So we need to do some book-keeping
								ColumnInfo newCI = ci.clone();
								newCI.colName = newColName;
								newCI.javaClass = newJavaClass;
								newCI.colType = newColType;
								
								newColInfos.put(newColName, newCI);
								colsToRemove.add(ci.colName);
							} else {
								ci.javaClass = newJavaClass;
								ci.colType = newColType;
							}
						}
					}
				}
				
				// Replace old col names with new ones
				for (String oldCols : colsToRemove)
					ti.colMap.remove(oldCols);
				ti.colMap.putAll(newColInfos);
				
				DebugOff(null);
			}
		}
	
		@SuppressWarnings("unchecked")
		public void scan(final Object cls, Store store) {
			final String className = getMetadataAdapter().getClassName(cls);
			List<String> annots = getMetadataAdapter().getClassAnnotationNames(cls);
			for (String annotationType : annots) {
				if (acceptResult(annotationType, cls) || annotationType.equals(Inherited.class.getName())) { 
					put(store, annotationType, className);
				}
			}
		}

		@SuppressWarnings("unchecked")
		public boolean acceptResult(final String fqn, Object cls) {
			if (((ClassFile) cls).getName().endsWith("NegativeInvoiceMigrationMonitor"))
				Utilities.Debug();
			String className = getMetadataAdapter().getClassName(cls);
			if (interestingTableAnnotations.contains(fqn)) {
				if (className.endsWith("Subscription"))
					Utilities.Debug();
				cc.putMappedClass(className, (ClassFile)cls);
				return true;
			} 
			return false;
		}
	}

	
	@SuppressWarnings("unused")
	public static void LoadFromAnnotations(Meta meta, ClassContinuum cc, String dirName, Predicate<String> p)
			throws ClassNotFoundException, IOException {

		long start = System.nanoTime();
		long end;

		// Get List of files to scan
		List<File> files = Utilities.listFilesRecursive(dirName, 
				(Path x) -> { return Files.isRegularFile(x) && !x.toString().toLowerCase().contains("test"); }, 					
				".jar");		

		// Set-up JavaAssist based scanner
		int jarCount = 0;
		List<URL> urlsToScan = new ArrayList<>();
		for (File file : files) {
			String pathName = file.getPath();
			String urlName = "file:" + pathName;
			Collection<URL> url = ClasspathHelper.forManifest(new URL(urlName));
			urlsToScan.addAll(url);
			jarCount++;
		}
		// Initialize scanner
		SideEffectingTypeAnnotationsScanner sets = new SideEffectingTypeAnnotationsScanner(meta, cc);
		
		// Load classes from Jar Files by Reflection
		Reflections reflections = new Reflections(
				new ConfigurationBuilder()
					.setUrls(urlsToScan)
					.setScanners(
							sets, 
							new GenericScanner(cc), 
							new FieldAnnotationsScanner()));
				
		// Resolve Pending Classes from the Meta resolving inheritance hierarchy
		sets.processPendingClasses();
		
		// Then Post-process classes from the Jars converting them into TableInfo structures
		sets.postProcessClasses();

		// Finally resolve column names
		sets.resolveColumnNames();
		
		// Done
		end = System.nanoTime();
		System.out.printf("Loaded Annotations from %d jars from dir: %s in %8.3f ms\n", jarCount, dirName, ((double)end - (double)start)/1e6);

	}
}
