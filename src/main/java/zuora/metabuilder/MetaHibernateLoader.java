package zuora.metabuilder;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.base.Strings;

import zuora.metabuilder.Meta.ColumnInfo;
import zuora.metabuilder.Meta.State;
import zuora.metabuilder.Meta.TableInfo;
import zuora.metabuilder.Meta.Typedef;
import zuora.metabuilder.Utilities.Tuple;

public class MetaHibernateLoader {
	static boolean _debug = false;
	public static void DebugOn (String className) { _debug = true; }
	public static void DebugOff(String className) { _debug = false; }
	
	private static String getJavaTypeFromHbmType(String type, Map<String,Typedef> typedefs) {
//		if (typedefs.containsKey(type))
//			return typedefs.get(type).clazz;
		
		switch (type.toLowerCase()) {
			case "text" 	 : return String.class.getName();
			case "varchar" 	 : return String.class.getName();
			case "string" 	 : return String.class.getName();
			case "timestamp" : return Timestamp.class.getName();
			case "int" 		 : return Integer.class.getName();
			case "integer" 	 : return Integer.class.getName();
			case "long" 	 : return Long.class.getName();
			case "short" 	 : return Short.class.getName();
			case "float"  	 : return Float.class.getName();
			case "double"	 : return Double.class.getName();
			case "char"		 : return Character.class.getName();
			case "uuid"		 : return UUID.class.getName();
			case "boolean"	 : return Boolean.class.getName();
			case "date"	 	 : return Date.class.getName();
			case "decimal"	 : return BigDecimal.class.getName();
			case "datetime" : return java.sql.Date.class.getName();
			default: {
				try {
					Class<?> clazz = Class.forName(type);
					return clazz.getName();
				} catch (ClassNotFoundException | NoClassDefFoundError ex) {
					return type;
				}
			}
		}
	}

	private static String getSqlTypeFromHbmType(String type, Map<String,Typedef> typedefs) {
		if (typedefs.containsKey(type))
			return typedefs.get(type).clazz;
		
		switch (type.toLowerCase()) {
			case "text" 	 : return "text";
			case "varchar" 	 : return "varchar(32)";
			case "string" 	 : return "varchar(32)";
			case "timestamp" : return "timestamp";
			case "int" 		 : return "int(11)";
			case "integer" 	 : return "int";
			case "long" 	 : return "bigint";
			case "short" 	 : return "smallint";
			case "float"  	 : return "float";
			case "double"	 : return "double";
			case "char"		 : return "char";
			case "uuid"		 : return "varchar(32)";
			case "boolean"	 : return "bit(1)";
			case "date"	 	 : return "datetime";
			case "decimal"	 : return "decimal(22,9)";
			case "datetime"  : return "datetime";
			default: {
				System.out.printf("Cannot reolve HBM type: %s\n", type);
				return null;
			}
		}
	}
	
	private static void fillTypeFromPropertyNode(TableInfo ti, Element element, ColumnInfo ci, Map<String,Typedef> typedefs) {
		
		if (ti.javaClass.equals("com.zuora.zbilling.rateplan.model.RatePlanChargeTier"))
			Utilities.Debug();
		
		
		// First Pick up the type from either the "type" or "sql-type" tag 
		int debug = 0;
		do {
			String specType = element.getAttribute("type");
			if (Strings.isNullOrEmpty(specType)) {
				NodeList nodes = element.getElementsByTagName("type");
				if (nodes.getLength() > 0) {
					Element typeNode = (Element)nodes.item(0);
					specType = typeNode.getAttribute("name");
				}
			}
			if (Strings.isNullOrEmpty(specType)) {
				specType = element.getAttribute("sql-type");
				if (Strings.isNullOrEmpty(specType)) {
					NodeList nodes = element.getElementsByTagName("column");
					if (nodes.getLength() > 0) {
						Element typeNode = (Element)nodes.item(0);
						specType = typeNode.getAttribute("sql-type");
					}
				}		
			}
			
			// Check if there is translation through the typedefs
			String javaClass = "";
			if (typedefs.containsKey(specType))
				javaClass = typedefs.get(specType).clazz;
			else
				javaClass = specType;
	
			if (javaClass.contains(".")) {
				ci.javaClass = javaClass;
			} else {
				ci.colType   = getSqlTypeFromHbmType(specType, typedefs);
				ci.javaClass = getJavaTypeFromHbmType(javaClass, typedefs);
			}
		} while (debug != 0);
	}

	private static String getTypeFromManyOneNode(Element element) {
		String type = element.getAttribute("class");
		if (!Strings.isNullOrEmpty(type))
			return type;

		NodeList nodes = element.getElementsByTagName("type");
		if (nodes.getLength() > 0) {
			Element typeNode = (Element)nodes.item(0);
			type = typeNode.getAttribute("name");
		}		
		return type;
	}
	
	private static void extractManyOneProps(TableInfo ti, Element element, File xmlFile, String source, Map<String, Typedef> typedefs) {
		Map<String, Map<String, Object>> props = new HashMap<>();
		
		ColumnInfo ci = null;
		String colName = null;
		String type = getTypeFromManyOneNode(element);
		
		NodeList nodes = element.getElementsByTagName("column");
		if (nodes.getLength() > 0) {
			colName = ((Element)(nodes.item(0))).getAttribute("name");
		} else {
			colName = element.getAttribute("column"); 
		}
		
		if (!Strings.isNullOrEmpty(colName)) {
			Tuple<Boolean, ColumnInfo> r = Utilities.getOrCreateColumnInfo(ti, colName, type, props, State.ResolvedByXML, source);
			ci = r.b;
    		Utilities.putColumnNameAnnotation(ci, colName);
    		
    		Map<String, Object> mapToOneAnnotations = ci.annotations.get(Meta.c_ManyOneAnnotationProperty);
    		if (mapToOneAnnotations == null) {
    			mapToOneAnnotations = new HashMap<>();
    			ci.annotations.put(Meta.c_ManyOneAnnotationProperty, mapToOneAnnotations);
    		}
    		mapToOneAnnotations.put(Meta.c_ManyOneClassProperty, element.getAttribute("class"));
		} else {
			Utilities.Debug();
		}
	}
	
	private static void extractPropertyProps(TableInfo ti, Element element, File xmlFile, String source, Map<String, Typedef>  typedefs) {
		Map<String, Map<String, Object>> props = new HashMap<>();

		// Find the column name
		ColumnInfo ci = null;
		String colName = null;
		Integer length = 0;
		Boolean isNullable = true;
		
		if (ti.tableName.equals("rateplancharge_tier_for_ds"))
			Utilities.Debug();
				
		NodeList nodes = element.getElementsByTagName("column");
		if (nodes.getLength() > 0) {
			Element columnNode = (Element)nodes.item(0);
			colName = columnNode.getAttribute("name");

			String _length  = columnNode.getAttribute("length");
			if (!Strings.isNullOrEmpty(_length) && Utilities.stringIsOnlyDigits(_length))
				length = Integer.parseInt(_length);

			String _notNull  = columnNode.getAttribute("not-null");
			isNullable = !Boolean.parseBoolean(_notNull);
		} else {
			colName = element.getAttribute("column"); 

			String _length  = element.getAttribute("length");
			if (!Strings.isNullOrEmpty(_length) && Utilities.stringIsOnlyDigits(_length))
				length = Integer.parseInt(_length);

			String _notNull  = element.getAttribute("not-null");
			isNullable = !Boolean.parseBoolean(_notNull);
		}
		
		if (!Strings.isNullOrEmpty(colName)) {
			Tuple<Boolean, ColumnInfo> r = Utilities.getOrCreateColumnInfo(ti, colName, "", props, State.ResolvedByXML, source);
			ci = r.b;
			ci.length = length;
			ci.isNullable = isNullable;
			if (ci.colName.startsWith("currency_id"))
				Utilities.Debug();
			fillTypeFromPropertyNode(ti, element, ci, typedefs);
			if (ci.colName.startsWith("currency_id"))
				System.out.printf("Seen currency_id: %s(%s:%s)\n", ti.javaClass, ci.colType, ci.javaClass);
			Utilities.putColumnNameAnnotation(ci, colName);
		} else {
			Utilities.Debug();
		}
	}
	
	private static void extractIdProps(TableInfo ti, Element element, File xmlFile, String source, Map<String, Typedef>  typedefs) {
		Map<String, Map<String, Object>> props = new HashMap<>();
		// Find the column name
		ColumnInfo ci = null;
		String colName = null;

		NodeList nodes = element.getElementsByTagName("column");
		if (nodes.getLength() > 0) {
			colName = ((Element)(nodes.item(0))).getAttribute("name");
		} else {
			colName = element.getAttribute("column"); 
		}

		if (!Strings.isNullOrEmpty(colName)) {
			Tuple<Boolean, ColumnInfo> r = Utilities.getOrCreateColumnInfo(ti, colName, "", props, State.ResolvedByXML, source);
			ci = r.b;
			ci.isPrimaryKey = true;
			fillTypeFromPropertyNode(ti, element, ci, typedefs);
			ti.pkColumns.add(colName);
			Utilities.putColumnNameAnnotation(ci, colName);
		} else {
			Utilities.Debug();
		}
	}
	
	public static void processClassNode(Meta meta, Node _classNode, File xmlFile, String source, Map<String, Typedef> typedefs) {

		
		if (xmlFile.toString().equals("/Users/tanmoyd/billinghbms/SfdcSyncState.hbm.xml"))
			Utilities.Debug();		

		
		Element classNode = (Element)_classNode;
		
		// Get class attributes we are interested in
		String tableName = classNode.getAttribute("table");
		String className = classNode.getAttribute("name");

		TableInfo ti = Utilities.getOrCreateTableInfo(meta, tableName, className, source);
		ti.state = State.ResolvedByXML;
		
		// For debugging
		if (Diagnostics.trackClass(className,  (x)->{DebugOn(x);}))
			Utilities.Debug();
		
		// Now iterate through id, properties and many-to-one nodes picking up the attributes of interest
		NodeList classChildren = classNode.getChildNodes();
		for (int i = 0; i < classChildren.getLength(); i++) {
			Node classChild = classChildren.item(i);
			
			// Do not need to look at the #text children
			if (classChild.getNodeType() != Node.ELEMENT_NODE)
				continue;

			Element element = (Element)classChild;
			String  elementName = element.getNodeName().toLowerCase();

			if (elementName.equals("id") || 
				elementName.equals("property") || 
				elementName.equals("many-to-one")) {
				switch (elementName) {
					case "id":
						extractIdProps(ti, element, xmlFile, source, typedefs);
						break;
					case "many-to-one":
						extractManyOneProps(ti, element, xmlFile, source, typedefs);
						break;
					case "property":
						extractPropertyProps(ti, element, xmlFile, source, typedefs);
						break;
					default:
						break;
				}
			}
		}
		
		Utilities.Debug();
		
		// Print
		// System.out.printf("Hibernate : [%s] [%s] Properties:%d\n", ti.tableName, ti.javaClass, ti.colMap.size());
	}

	public static void loadHibernateFile(Meta meta, File xmlFile, String source) throws SAXException, IOException, ParserConfigurationException {
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    dbf.setValidating(false);
	    dbf.setNamespaceAware(true);
	    dbf.setFeature("http://xml.org/sax/features/namespaces", false);
	    dbf.setFeature("http://xml.org/sax/features/validation", false);
	    dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
	    dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	    
	    DocumentBuilder dBuilder = dbf.newDocumentBuilder();
	    Document doc = dBuilder.parse(xmlFile);
	    doc.getDocumentElement().normalize();
	    NodeList nList = doc.getElementsByTagName("hibernate-mapping");
	    if (nList.getLength() == 0)
	    	return;
	    
	    // Get List of typedef nodes
	    List<Element> _typedefs = new ArrayList<>();
	    Utilities.getListOfElementsByName((Element)nList.item(0), "typedef", _typedefs);
	    
	    Map<String, Typedef> typedefs = new HashMap<>();
	    for (Element e : _typedefs) {
	    	String typededName = (String)e.getAttribute("name");
	    	Typedef typedefNode = new Typedef(typededName, (String)e.getAttribute("class"));
	    	
	    	List<Element> params = new ArrayList<>();
	    	Utilities.getListOfElementsByName(e, "param", params);
	    	for (Element p : params) {
	    		String param = p.getAttribute("name");
	    		String child = p.getTextContent();
	    		typedefNode.params.put(param, child);
	    	}
	    	typedefs.put(typededName, typedefNode);
	    }
	    
	    // Get Class element
	    if (nList != null) {
	    	Node hmNode = nList.item(0);
	    	NodeList children = hmNode.getChildNodes();
	    	boolean fResolved = false;
	    	
	    	for (int i = 0; i < children.getLength(); i++) {
	    		Node child = children.item(i);
	    		if (child.getNodeName().toLowerCase().equals("class")) {
	    			processClassNode(meta, child, xmlFile, source, typedefs);
	    			fResolved = true;
	    		}
	    	}
	    	
	    	if (!fResolved)
	    		Utilities.Debug();
	    }
	}
	
	public static void postProcessClasses() {
	}
	
	public static void LoadFromHibernateFiles(Meta meta, String dirName, Predicate<String> p) throws SAXException, IOException, ParserConfigurationException {

		// Get List of files to scan
		List<File> files = Utilities.listFilesRecursive(dirName, 
				(Path x) -> { return Files.isRegularFile(x) && !x.toString().toLowerCase().contains("test"); }, 					
				".hbm.xml");
		
		// Set-up JavaAssist based scanner
		long start = System.nanoTime();
		long end;
		
		long fileCount = 0;
		for (File file : files) {
			try {
				loadHibernateFile(meta, file, file.toString());
				fileCount++;
			} catch (Exception ex) {
				ex.printStackTrace();
				System.out.printf("Skipping file: %s\n", file);
			}
		}
		
		// Post Process Classes
		postProcessClasses();
		end = System.nanoTime();
		System.out.printf("Loaded %d Hibernate Files from dir: %s in %8.3f ms\n", fileCount, dirName, ((double)end - (double)start)/1e6);
	}
}
