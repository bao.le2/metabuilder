package zuora.metabuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.zuora.sidecar.metadata.configuration.BusinessDataConfiguration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;

public class MetaViewLoader {

	public static ObjectMapper configureObjectMapper(ObjectMapper objectMapper) {

		// ignore unknown fields (for backwards compatibility)
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		// use ISO dates
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

		// skip fields that are null instead of writing an explicit json null value
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_ABSENT);

		// disable auto detection of json properties... all properties must be explicit
		objectMapper.disable(MapperFeature.AUTO_DETECT_CREATORS);
		objectMapper.disable(MapperFeature.AUTO_DETECT_FIELDS);
		// TODO(OWL-2367): Re-enable this setting
		// objectMapper.disable(MapperFeature.AUTO_DETECT_SETTERS);
		objectMapper.disable(MapperFeature.AUTO_DETECT_GETTERS);
		objectMapper.disable(MapperFeature.AUTO_DETECT_IS_GETTERS);
		objectMapper.disable(MapperFeature.USE_GETTERS_AS_SETTERS);
		objectMapper.disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);
		objectMapper.disable(MapperFeature.INFER_PROPERTY_MUTATORS);
		objectMapper.disable(MapperFeature.ALLOW_FINAL_FIELDS_AS_MUTATORS);

		objectMapper.enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);

		return objectMapper;
	}

	public static void LoadFromViewDefinitions(Meta meta, String dirName) throws IOException {

		YAMLFactory yf = new YAMLFactory();
		yf.disable(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.USE_NATIVE_TYPE_ID);
		yf.disable(com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature.WRITE_DOC_START_MARKER);

		ObjectMapper mapper = configureObjectMapper(io.dropwizard.jackson.Jackson.newObjectMapper(
				new YAMLFactory()
				.disable(Feature.USE_NATIVE_TYPE_ID)
				.disable(Feature.WRITE_DOC_START_MARKER)));

		// Get List of files to scan
		List<File> files = Utilities.listFilesRecursive(dirName, (Path x) -> {
			return Files.isRegularFile(x) && !x.toString().toLowerCase().contains("test");
		}, ".yaml");

		for (File file : files) {
			BusinessDataConfiguration bdc = mapper.readValue(file, BusinessDataConfiguration.class);
			System.out.println("=======================================================================");
			System.out.println(bdc);
			System.out.println("=======================================================================");
		}
	}

	public static void main(String[] args) {
		try {
			LoadFromViewDefinitions(null, "/Users/ble/IntelliJ/sidecar/sidecar-db/src/main/resources/descriptors");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
