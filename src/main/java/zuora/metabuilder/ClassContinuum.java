package zuora.metabuilder;

import java.util.HashMap;
import java.util.Map;

import javassist.bytecode.ClassFile;

public class ClassContinuum {
	private Map<String, ClassFile> mappedClasses    = new HashMap<>();
	private Map<String, ClassFile> augmentedClasses = new HashMap<>();

	public Map<String, ClassFile> getMappedClasses() {
		return mappedClasses;
	}
	public ClassFile lookupClass(String className) {
		ClassFile o = mappedClasses.get(className);
		if (o != null)
			return o;
		else
			return augmentedClasses.get(className);
	}
	
	public boolean isMapped(String className) {
		return mappedClasses.containsKey(className);
	}
	
	public void putMappedClass(String className, ClassFile clsFile) {
		mappedClasses.put(className, clsFile);
	}
	
	public void putAugmentedClass(String className, ClassFile clsFile) {
		augmentedClasses.put(className, clsFile);
	}
}
