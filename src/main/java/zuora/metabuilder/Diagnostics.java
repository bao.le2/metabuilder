package zuora.metabuilder;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class Diagnostics {
	static Set<String> classesToTrack = new HashSet<>();
	static String[] _classesToTrack = {
//			"com.zuora.zbilling.subscription.model.Subscription",
			"com.zuora.zbilling.paymentMethod.model.PaymentMethod",
//			"com.zuora.order.model.Order"
	};
	static {
		for (String s : _classesToTrack)
			classesToTrack.add(s);
	}
	
	public static boolean trackClass(String className, Consumer<String> c) {
		if (classesToTrack.contains(className)) {
			Utilities.Debug();
			if (c != null)
				c.accept(className);
			return true;
		}
		return false;
	}
}
