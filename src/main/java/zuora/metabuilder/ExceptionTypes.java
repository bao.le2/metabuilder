package zuora.metabuilder;

import java.util.HashMap;
import java.util.Map;

public class ExceptionTypes {
	public static class Exceptions {
		String category;
		String javaClass;
		String colType;
		public Exceptions(String category, String javaClass, String colType) {
			this.category = category;
			this.javaClass = javaClass;
			this.colType = colType;
		}
	}
		
	private static Exceptions[] exceptionTypes = {
			new Exceptions("enum", "class com.zuora.zbilling.memo.model.migration.MigrationProgress", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationJobType", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationJobStatus", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationPreCheckResultType", "smallint(1)"),
			new Exceptions("enum", "class com.zuora.shared.auth.model.AuthorizationMigrationStatus", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zbilling.multipartjob.model.AsyncExecutionJob$JobStatus", "varchar(60)"),
			new Exceptions("enum", "class com.zuora.zbilling.setting.model.DocTemplateCategory", "smallint(2)"),
			new Exceptions("enum", "class com.zuora.zbilling.setting.model.DocTemplateType", "smallint(2)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningLogEvent", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.enums.UserStatus", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEvent", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.job.model.OrderJobStatus", "bigint(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.memo.model.migration.MigrationProgress", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationVerificationJobType", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationVerificationFieldName", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationVerificationJobStatus", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationVerificationFieldName", "int(1)"),
			new Exceptions("enum", "class com.zuora.zbilling.subscription.migration.model.enums.OrderMigrationVerificationResultType", "int(1)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCBulkProvisioningType", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.shared.auth.model.AuthorizationMigrationStatus", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zbilling.multipartjob.model.MultiPartPayload$Status", "varchar(20)"),
			new Exceptions("enum", "class com.zuora.zbilling.memo.model.migration.MigrationProgress", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(100)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEvent", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.enums.TenantStatus", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.zconnect.provision.enumeration.ZCProvisioningEventState", "varchar(32)"),
			new Exceptions("enum", "class com.zuora.settlement.transaction.enums.ARTransactionType", "varchar(32)"),
			
			new Exceptions("general", "com.zuora.hibernate.usertype.NumberTransfer", "int(11)"),
			new Exceptions("general", "cm.zuora.zbilling.paymentgateway.model.PaymentGatewayUserType", "varchar(32)"),
			new Exceptions("general", "com.zuora.hibernate.usertype.NumberTransfer", "varchar(32)"),
			new Exceptions("general", "com.zuora.datasource.model.MQuery", "varchar(20000)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType", "varchar(70)"),
			new Exceptions("general", "com.zuora.hibernate.usertype.OrderbyTransfer", "varchar(500)"),
			new Exceptions("general", "com.kenoah.enums.BitSetTransfer", "int(11)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType", "varchar(70)"),
			new Exceptions("general", "com.zuora.fieldtrail.FieldTrailEntityTransfer", "varchar(1500)"),
			new Exceptions("general", "com.zuora.objectserialization.ObjectSerializerUserType",  "text"),
			new Exceptions("general", "com.zuora.admin.permission.ApplicationPermissionsType",  "int(10) unsigned"),
			new Exceptions("general", "com.zuora.enums.PaymentMethodType",  "varchar(256)"),
			new Exceptions("general", "com.zuora.objectserialization.ObjectSerializerUserType",  "varchar(2000)"),
			new Exceptions("general", "com.zuora.admin.user.model.CustomJobFunction",  "varchar(256)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "varchar(32)"),
			new Exceptions("general", "com.zuora.sync.sfdc.model.BatchModeEnumType",  "varchar(256)"),
			new Exceptions("general", "com.zuora.admin.preference.ApplicationPreferences",  "varchar(256)"),
			new Exceptions("general", "com.zuora.objectserialization.ObjectSerializerUserType",  "varchar(255)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "tinyint(4)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "varchar(32)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "varchar(70)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "varchar(70)"),
			new Exceptions("general", "com.zuora.admin.permission.ApplicationPermissionsType",  "int(10) unsigned"),
			new Exceptions("general", "org.hibernate.type.LocaleType",  "varchar(32)"),
			new Exceptions("general", "com.zuora.zpayment.paymentmethod.paymentmethodtype.PaymentMethodTypeDbType",  "varchar(70)"),
			new Exceptions("general", "com.zuora.zbilling.settingchange.enums.SettingItem",  "tinyint(4)"),
			new Exceptions("general", "com.zuora.global.shard.model.TenantCategories",  "varchar(256)"),
			new Exceptions("general", "com.zuora.enums.BaseEnumType",  "varchar(11)"),
			new Exceptions("general", "org.hibernate.type.EnumType",  "varchar(11)"),
			new Exceptions("general", "com.zuora.base.DecimalType",  "numeric"),
			new Exceptions("general", "com.zuora.base.timezone.ZDateType",  "datetime"),
			new Exceptions("general", "com.zuora.framework.model.Entity",  "varchar(32)"),
			new Exceptions("general", "com.zuora.infra.crypto.encryptedstring.EncryptedString",  "varchar(256)"),
			new Exceptions("general", "com.zuora.infra.crypto.encryptedstring.EncryptedStringType",  "varchar(500)"),
			new Exceptions("general", "com.kenoah.enums.BitSet",  "int(10)"),	
			new Exceptions("general", "com.zuora.enums.EnumByteTransfer",  "tinyint(4) unsigned"),	
			new Exceptions("general", "com.zuora.base.timezone.ZDateType",  "datetime"),
			new Exceptions("general", "com.zuora.zbilling.paymentgateway.gatewaySDK.enumration.GWTransactionType", "varchar(32)"),
			new Exceptions("general", "com.zuora.zbilling.paymentgateway.model.PaymentGatewayUserType", "varchar(32)"),
			new Exceptions("general", "com.zuora.sync.sfdc.model.SfdcSyncState", "char(1)"),
			new Exceptions("general", "com.zuora.zbilling.paymentgateway.model.PaymentGatewayUserTypeMutable", "varchar(20)"),
			new Exceptions("general", "com.zuora.metrics.service.model.AsyncReconciliationBatch$AsyncReconciliationBatchStatus", "int(11)"),
			new Exceptions("general", "status:com.zuora.metrics.api.AlfdMigrationJobStatus", "int(11)"),
			
			new Exceptions("general", "com.zuora.metrics.api.MigrationJobStatus", "int(11)"),
			new Exceptions("general", "com.zuora.metrics.api.ReconciliationJobStatus", "int(11)"),
			new Exceptions("general", "status:com.zuora.metrics.service.model.AsyncMigrationBatch$AsyncMigrationBatchStatus", "int(11)"),
			new Exceptions("general", "com.zuora.metrics.api.ListPriceBase", "decimal(22,9)"),
			new Exceptions("general", "com.zuora.metrics.api.DataExtractJobStatus", "int(11)"),
			new Exceptions("general", "com.zuora.metrics.api.DiscountType", "int(11)"),
			
			new Exceptions("general", "com.zuora.metrics.api.AmendmentType", "int(11)"),
			new Exceptions("general", "com.zuora.metrics.api.MetricType", "int(11)"),
			new Exceptions("general", "com.zuora.metrics.service.model.AsyncReconciliationBatch$Subscriptions", "int(11)"),
			
			new Exceptions("foldintologic", "com.zuora.enums.IntegerEnumType",  "int(11)"),
			new Exceptions("foldintologic", "com.zuora.enums.StringEnumType",  "varchar(32)"),
	};

	private static Map<String, Exceptions> exceptionsMap = new HashMap<>();
	static {
		for (Exceptions e : exceptionTypes) {
			exceptionsMap.put(e.javaClass, e);
		}
	}
	
	public static String getColTypeFromExceptions(String javaClass) {
		Exceptions e = null;
		if ((e = exceptionsMap.get(javaClass)) != null) {
			return e.colType;
		}
		return null;
	}
}
